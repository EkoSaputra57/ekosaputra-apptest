const initialState = {
  contactData: [],
  isLoading: false,
};

const contact = (state = initialState, action) => {
  switch (action.type) {
    case 'GET_CONTACT_DATA':
      return {
        ...state,
        isLoading: true,
      };
    case 'GET_CONTACT_DATA_SUCCESS':
      return {
        ...state,
        contactData: action.data,
        isLoading: false,
      };
    case 'CONTACT_DATA_EDIT':
      return {
        ...state,
        isLoading: true,
      };
    case 'CONTACT_DATA_EDIT_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'CONTACT_DATA_EDIT_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'CONTACT_DATA_DELETE':
      return {
        ...state,
        isLoading: true,
      };
    case 'CONTACT_DATA_DELETE_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'CONTACT_DATA_DELETE_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    case 'CONTACT_DATA_CREATE':
      return {
        ...state,
        isLoading: true,
      };
    case 'CONTACT_DATA_CREATE_SUCCESS':
      return {
        ...state,
        isLoading: false,
      };
    case 'CONTACT_DATA_CREATE_FAILED':
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default contact;
