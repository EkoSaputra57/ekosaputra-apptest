import axios from 'axios';
import {takeLatest, put} from '@redux-saga/core/effects';
import {Alert} from 'react-native';

function* getContactData(action) {
  try {
    console.log('actionnya', action);
    const resContactData = yield axios.get(
      `https://simple-contact-crud.herokuapp.com/contact`,
    );
    yield put({
      type: 'GET_CONTACT_DATA_SUCCESS',
      data: resContactData.data.data,
    });
  } catch (err) {
    console.log('>>>', err);
  }
}

function* createContactData(action) {
  try {
    console.log('create Contact data', action);
    const resCreateContactData = yield axios({
      method: 'POST',
      url: 'https://simple-contact-crud.herokuapp.com/contact',
      data: action.data,
    });
    if (resCreateContactData && resCreateContactData.data) {
      console.log(resCreateContactData.data);
      yield put({type: 'CONTACT_DATA_CREATE_SUCCESS'});
      yield put({type: 'GET_CONTACT_DATA'});
      Alert.alert('Status:', 'Create Contact data Success', [
        {text: 'Okay', onPress: () => action.navigation()},
      ]);
    }
    console.log('Create Contact Success');
  } catch (err) {
    console.log(err);
    yield put({type: 'CONTACT_DATA_CREATE_FAILED'});
    Alert.alert('Status:', 'Create Contact data Failed, Please try Again', [
      {text: 'Okay'},
    ]);
  }
}

function* editContactData(action) {
  console.log(action);
  try {
    console.log('edit Contact data start');
    const resEditContactData = yield axios({
      method: 'PUT',
      url: `https://simple-contact-crud.herokuapp.com/contact/${action.id}`,
      data: action.data,
    });
    console.log('edit Contact data success');
    yield put({
      type: 'CONTACT_DATA_EDIT_SUCCESS',
    });
    yield put({type: 'GET_CONTACT_DATA'});
    Alert.alert('Status:', 'Edit Contact Data Success', [
      {
        text: 'okay',
        onPress: () => action.navigation(),
      },
    ]);
  } catch (error) {
    console.log(err);
    yield put({
      type: 'CONTACT_DATA_EDIT_FAILED',
    });
    Alert.alert(
      'Status: Failed',
      'Please try Again by update all of the data',
      [{text: 'Okay'}],
    );
  }
}

function* deleteContactData(action) {
  console.log('-->', action);
  try {
    console.log('memulai delete Contact data');
    const resDeleteContactData = yield axios({
      method: 'delete',
      url: `https://simple-contact-crud.herokuapp.com/contact/${action.id}`,
    });
    console.log('delete Contact data success');
    yield put({type: 'CONTACT_DATA_DELETE_SUCCESS'});
    yield put({type: 'GET_CONTACT_DATA'});
    Alert.alert('Status:', 'Delete Contact Data Success', [
      {
        text: 'okay',
      },
    ]);
  } catch (err) {
    console.log(err);
    yield put({type: 'CONTACT_DATA_DELETE_FAILED'});
    Alert.alert('Status: Failed', 'Contact data failed to delete', [
      {text: 'Okay'},
    ]);
  }
}

export default function* contactSaga() {
  yield takeLatest('GET_CONTACT_DATA', getContactData);
  yield takeLatest('CONTACT_DATA_CREATE', createContactData);
  yield takeLatest('CONTACT_DATA_EDIT', editContactData);
  yield takeLatest('CONTACT_DATA_DELETE', deleteContactData);
}
