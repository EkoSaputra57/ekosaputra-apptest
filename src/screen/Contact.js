import React, {useEffect} from 'react';
import {useNavigation} from '@react-navigation/native';
import {
  View,
  Text,
  ImageBackground,
  ActivityIndicator,
  StyleSheet,
  FlatList,
} from 'react-native';
import Header from '../component/Header';
import Card from '../component/ContactCard';
import {useDispatch, useSelector} from 'react-redux';
import CreateButton from '../component/CreateButton';

const Contact = () => {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const contact_redux = useSelector(state => state.contact.contactData);

  useEffect(() => {
    dispatch({type: 'GET_CONTACT_DATA'});
  }, []);

  const renderContact = ({item, index}) => {
    return (
      <Card
        firstName={item.firstName}
        lastName={item.lastName}
        age={item.age}
        image={item.photo}
        id={item.id}
        onPressDelete={() => handleDelete(item)}
      />
    );
  };

  const handleDelete = data => {
    console.log(data);
    dispatch({
      type: 'CONTACT_DATA_DELETE',
      id: data.id,
    });
  };

  useEffect(() => {}, [contact_redux]);

  return (
    <ImageBackground
      resizeMode="cover"
      style={{flex: 1}}
      source={require('../asset/BG.jpg')}>
      <View style={style.BG}>
        <Header title="Contact List" />
        <CreateButton
          onPress={() => navigation.navigate('CreateContact')}
          act="Create New"
          icon="pluscircle"
        />
        {contact_redux.length == 0 ? (
          <ActivityIndicator size="large" color="grey" />
        ) : (
          <FlatList
            data={contact_redux}
            keyExtractor={(elem, i) => i}
            renderItem={renderContact}
          />
        )}
      </View>
    </ImageBackground>
  );
};

export default Contact;

const style = StyleSheet.create({
  BG: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flex: 1,
  },
});
