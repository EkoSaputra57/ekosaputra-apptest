import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import Header from '../component/Header';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import CreateButton from '../component/CreateButton';
import TxtInput from '../component/TxtInput';
import InputNum from '../component/InputNum';
import ImagePicker from 'react-native-image-picker';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {useRoute} from '@react-navigation/native';

const EditContact = () => {
  const [photo, setPhoto] = useState();
  const route = useRoute();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const loading = useSelector(state => state.contact.isLoading);
  const id = route.params.id;
  const prevFN = route.params.firstName;
  const prevLN = route.params.lastName;
  const prevAge = route.params.age;
  const prevPhoto = route.params.image;
  const [photoDis, setPhotoDis] = useState();
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [age, setAge] = useState();
  const options = {
    title: 'Select Avatar',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  const PrevAge = prevAge.toString();
  function pickImage() {
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {
          uri: response.uri,
          type: response.type,
          data: response.data,
          name: response.fileName,
        };

        setPhotoDis(source.uri);
        setPhoto(source.uri);
      }
    });
  }

  const handleEdit = () => {
    let formData = new FormData();
    {
      photo && formData.append('photo', photo);
    }
    formData.append('firstName', `${firstName}`);
    formData.append('lastName', `${lastName}`);
    formData.append('age', `${age}`);
    dispatch({
      type: 'CONTACT_DATA_EDIT',
      data: formData,
      id: id,
      navigation: navigation.goBack,
    });
  };

  return (
    <ImageBackground
      resizeMode="cover"
      style={{flex: 1}}
      source={require('../asset/BG.jpg')}>
      <View style={style.BG}>
        <Header title="Edit Contact" />
        <ScrollView>
          <KeyboardAvoidingView
            style={{justifyContent: 'center', alignItems: 'center'}}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image
                source={
                  photoDis
                    ? {uri: photoDis}
                    : {
                        uri: prevPhoto,
                      }
                }
                style={{
                  height: 100,
                  width: 100,
                  borderRadius: 1000,
                  borderWidth: 1,
                  borderColor: '#E5E5E6',
                  marginTop: 20,
                  marginBottom: 10,
                }}
              />

              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  borderRadius: 20,
                  width: 135,
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 38,
                  borderColor: '#C9C9C9',
                  backgroundColor: 'violet',
                }}
                onPress={() => pickImage()}>
                <View style={{flexDirection: 'row'}}>
                  <View style={{marginRight: 5}}>
                    <AntDesign name="user" size={20} color="white" />
                  </View>

                  <Text style={{color: 'white', fontWeight: 'bold'}}>
                    Change image
                  </Text>
                </View>
              </TouchableOpacity>
            </View>

            <TxtInput
              title="First Name"
              onChangeText={text => setFirstName(text)}
              value={firstName}
              defaultValue={prevFN}
            />
            <TxtInput
              title="Last Name"
              onChangeText={text => setLastName(text)}
              value={lastName}
              defaultValue={prevLN}
            />
            <InputNum
              title="Age"
              onChangeText={text => setAge(text)}
              value={age}
              defaultValue={PrevAge}
            />
            <View style={{width: '90%', marginTop: 10, marginBottom: 30}}>
              {loading ? (
                <ActivityIndicator size="large" color="grey" />
              ) : (
                <CreateButton
                  onPress={() => handleEdit()}
                  act="Edit"
                  icon="right"
                />
              )}
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    </ImageBackground>
  );
};

export default EditContact;

const style = StyleSheet.create({
  BG: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flex: 1,
  },
});
