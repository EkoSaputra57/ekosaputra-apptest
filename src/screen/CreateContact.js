import React, {useState} from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import Header from '../component/Header';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import CreateButton from '../component/CreateButton';
import TxtInput from '../component/TxtInput';
import InputNum from '../component/InputNum';
import ImagePicker from 'react-native-image-picker';
import AntDesign from 'react-native-vector-icons/AntDesign';

const CreateContact = () => {
  const [photo, setPhoto] = useState();
  const [photoDis, setPhotoDis] = useState();
  const [firstName, setFirstName] = useState();
  const [lastName, setLastName] = useState();
  const [age, setAge] = useState();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const loading = useSelector(state => state.contact.isLoading);

  const options = {
    title: 'Select Avatar',
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };
  function pickImage() {
    ImagePicker.showImagePicker(options, response => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {
          uri: response.uri,
          type: response.type,
          data: response.data,
          name: response.fileName,
        };

        setPhotoDis(source.uri);
        setPhoto(source.uri);
      }
    });
  }

  const handleCreate = () => {
    let formData = new FormData();
    {
      photo && formData.append('photo', photo);
    }
    formData.append('firstName', `${firstName}`);
    formData.append('lastName', `${lastName}`);
    formData.append('age', `${age}`);
    dispatch({
      type: 'CONTACT_DATA_CREATE',
      data: formData,
      navigation: navigation.goBack,
    });
  };

  return (
    <ImageBackground
      resizeMode="cover"
      style={{flex: 1}}
      source={require('../asset/BG.jpg')}>
      <View style={style.BG}>
        <Header title="Create Contact" />
        <ScrollView>
          <KeyboardAvoidingView
            style={{justifyContent: 'center', alignItems: 'center'}}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Image
                source={
                  photoDis
                    ? {uri: photoDis}
                    : {
                        uri: 'https://i.pinimg.com/originals/43/cf/24/43cf24c49008db7d7900d4cf7fd62fd7.jpg',
                      }
                }
                style={{
                  height: 100,
                  width: 100,
                  borderRadius: 1000,
                  borderWidth: 1,
                  borderColor: '#E5E5E6',
                  marginTop: 20,
                  marginBottom: 10,
                }}
              />

              <TouchableOpacity
                style={{
                  borderWidth: 1,
                  borderRadius: 20,
                  width: 135,
                  alignItems: 'center',
                  justifyContent: 'center',
                  height: 38,
                  borderColor: '#C9C9C9',
                  backgroundColor: 'violet',
                }}
                onPress={() => pickImage()}>
                <View style={{flexDirection: 'row'}}>
                  <View style={{marginRight: 5}}>
                    <AntDesign name="user" size={20} color="white" />
                  </View>
                  {photoDis ? (
                    <Text style={{color: 'white', fontWeight: 'bold'}}>
                      Change image
                    </Text>
                  ) : (
                    <Text style={{color: 'white', fontWeight: 'bold'}}>
                      Add image
                    </Text>
                  )}
                </View>
              </TouchableOpacity>
            </View>

            <TxtInput
              title="First Name"
              onChangeText={text => setFirstName(text)}
              value={firstName}
            />
            <TxtInput
              title="Last Name"
              onChangeText={text => setLastName(text)}
              value={lastName}
            />
            <InputNum
              title="Age"
              onChangeText={text => setAge(text)}
              value={age}
            />
            <View style={{width: '90%', marginTop: 10, marginBottom: 30}}>
              {loading ? (
                <ActivityIndicator size="large" color="grey" />
              ) : (
                <CreateButton
                  onPress={() => handleCreate()}
                  act="Create New"
                  icon="pluscircle"
                />
              )}
            </View>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    </ImageBackground>
  );
};

export default CreateContact;

const style = StyleSheet.create({
  BG: {
    backgroundColor: 'rgba(52, 52, 52, 0.8)',
    flex: 1,
  },
});
