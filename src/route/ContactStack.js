import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Contact from '../screen/Contact';
import CreateContact from '../screen/CreateContact';
import EditContact from '../screen/EditContact';

const Stack = createStackNavigator();

const ContactStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Contact"
        component={Contact}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="CreateContact"
        component={CreateContact}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="EditContact"
        component={EditContact}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

export default ContactStack;
