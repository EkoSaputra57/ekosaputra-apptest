import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';

const InputNum = props => {
  return (
    <View style={style.container}>
      <Text style={style.title}>{props.title}</Text>
      <TextInput
        placeholder={props.placeholder}
        onChangeText={props.onChangeText}
        value={props.value}
        defaultValue={props.defaultValue}
        style={style.txtIn}
        keyboardType="numeric"
        textAlign="center"
      />
    </View>
  );
};

export default InputNum;

const style = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 20,
    alignItems: 'center',
  },
  title: {
    color: 'white',
    marginBottom: 5,
    fontSize: 18,
  },
  txtIn: {
    fontSize: 16,
    width: 80,
    backgroundColor: 'white',
    borderRadius: 9,
    fontWeight: 'bold',
    color: 'black',
  },
});
