import React from 'react';
import {View, Text, TextInput, StyleSheet} from 'react-native';

const TxtInput = props => {
  return (
    <View style={style.container}>
      <Text style={style.title}>{props.title}</Text>
      <TextInput
        placeholder={props.placeholder}
        onChangeText={props.onChangeText}
        value={props.value}
        defaultValue={props.defaultValue}
        style={style.txtIn}
      />
    </View>
  );
};

export default TxtInput;

const style = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginTop: 20,
    alignItems: 'center',
  },
  title: {
    color: 'white',
    marginBottom: 5,
    fontSize: 18,
  },
  txtIn: {
    fontSize: 16,
    width: 300,
    backgroundColor: 'white',
    borderRadius: 9,
    paddingRight: 10,
    paddingLeft: 10,
    color: 'black',
  },
});
