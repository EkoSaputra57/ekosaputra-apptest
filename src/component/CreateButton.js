import React from 'react';
import AntDesign from 'react-native-vector-icons/AntDesign';
import {View, Text, TouchableOpacity, StyleSheet} from 'react-native';

const CreateButton = props => {
  return (
    <View style={style.wrapper}>
      <TouchableOpacity style={style.container} onPress={props.onPress}>
        <Text style={style.txt}>{props.act} Contact</Text>
        <AntDesign name={props.icon} size={30} color="grey" />
      </TouchableOpacity>
    </View>
  );
};

export default CreateButton;

const style = StyleSheet.create({
  container: {
    paddingTop: 5,
    marginBottom: 10,
    marginTop: 10,
    paddingBottom: 5,
    width: '75%',
    backgroundColor: 'wheat',
    borderWidth: 1,
    borderRadius: 8,
    borderColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
  },
  txt: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 16,
  },
  wrapper: {justifyContent: 'center', alignItems: 'center'},
});
