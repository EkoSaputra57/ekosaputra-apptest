import React, {useState} from 'react';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  View,
  Text,
  Alert,
  TouchableOpacity,
  Image,
  StyleSheet,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

const Card = props => {
  const [showBox, setShowBox] = useState(true);
  const navigation = useNavigation();
  const showConfirmDialog = () => {
    return Alert.alert(
      'Delete Data',
      'Are you sure you want to delete this data?',
      [
        {
          text: 'Yes',
          onPress: () => {
            setShowBox(false);
            props.onPressDelete();
          },
        },

        {
          text: 'No',
        },
      ],
    );
  };

  return (
    <View style={{alignItems: 'center'}}>
      <TouchableOpacity style={style.card}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Image
            source={{
              uri: props.image
                ? props.image
                : props.image == 'N/A'
                ? 'https://i.pinimg.com/originals/43/cf/24/43cf24c49008db7d7900d4cf7fd62fd7.jpg'
                : 'https://i.pinimg.com/originals/43/cf/24/43cf24c49008db7d7900d4cf7fd62fd7.jpg',
            }}
            style={style.img}
            imageStyle={{borderRadius: 1000}}
          />
          <View>
            <Text style={style.title}>
              {props.firstName} {props.lastName}
            </Text>
            <View style={{flexDirection: 'row'}}>
              <Text style={{paddingLeft: 10, color: 'grey'}}>Age: </Text>
              <Text style={{color: 'black'}}>{props.age}</Text>
            </View>
          </View>
          <View>
            <TouchableOpacity>
              <MaterialCommunityIcons
                name="pencil"
                size={26}
                style={{color: 'red', marginBottom: 10, marginTop: 10}}
                onPress={() =>
                  navigation.navigate('EditContact', {
                    id: props.id,
                    firstName: props.firstName,
                    lastName: props.lastName,
                    age: props.age,
                    image: props.image,
                  })
                }
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <MaterialCommunityIcons
                name="delete"
                size={26}
                style={{color: 'red'}}
                onPress={() => showConfirmDialog()}
              />
            </TouchableOpacity>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default Card;

const style = StyleSheet.create({
  card: {
    paddingBottom: 10,
    backgroundColor: 'white',
    justifyContent: 'center',
    width: 360,
    borderRadius: 8,
    paddingLeft: 20,
    borderWidth: 1,
    borderColor: '#E5E5E5',
    marginTop: 5,
    marginBottom: 10,
  },
  img: {
    width: 60,
    height: 60,
    marginBottom: 5,
    marginTop: 5,
    borderRadius: 1000,
  },
  title: {
    color: 'rgb(31,169,239)',
    fontSize: 20,
    paddingLeft: 10,
    width: 240,
    justifyContent: 'center',
    fontWeight: 'bold',
  },
  com: {
    fontSize: 16,
    width: 300,
    fontWeight: 'bold',
    color: 'grey',
  },
});
