import React from 'react';
import SplashScreen from 'react-native-splash-screen';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import {store} from './src/redux/store';
import ContactStack from './src/route/ContactStack';

SplashScreen.hide();

const App = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <ContactStack />
      </NavigationContainer>
    </Provider>
  );
};

export default App;
